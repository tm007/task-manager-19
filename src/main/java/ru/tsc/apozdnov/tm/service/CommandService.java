package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.repository.ICommandRepository;
import ru.tsc.apozdnov.tm.api.service.ICommandService;
import ru.tsc.apozdnov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommand() {
        return commandRepository.getCommands();
    }

    @Override
    public void add(final AbstractCommand abstractCommand) {
        commandRepository.add(abstractCommand);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (name == null && name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String arg) {
        if (arg == null && arg.isEmpty()) return null;
        return commandRepository.getCommandByArgument(arg);
    }

}
