package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;

import ru.tsc.apozdnov.tm.api.service.IProjectService;

import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.DescriptionEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IndexIncorrectException;
import ru.tsc.apozdnov.tm.exception.field.NameEmptyException;
import ru.tsc.apozdnov.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescriprion(description);
        return project;
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescriprion(description);
        return project;
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Project create(final String name, String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Project create(final String name, final String description, final Date dateBegin, final Date dateEnd) {
        final Project project = create(name, description);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project add(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return repository.add(project);
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        return null;
    }

    @Override
    public void clear() {
        repository.clear();
    }

}
