package ru.tsc.apozdnov.tm.repository;

import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.model.Project;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(String name, String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescriprion(description);
        return add(project);
    }

}
