package ru.tsc.apozdnov.tm.repository;

import ru.tsc.apozdnov.tm.api.repository.IUserRepository;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.util.HashUtil;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(RoleType.USUAL);
        return add(user);
    }

    @Override
    public User findOneByLogin(final String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final RoleType role) {
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User findOneByEmail(final String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public boolean isLoginExist(final String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public boolean isEmailExist(final String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

}
