package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.enumerated.Status;

import ru.tsc.apozdnov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    List<Task> findAllByProjectId(String projectId);

    void clear();

}
