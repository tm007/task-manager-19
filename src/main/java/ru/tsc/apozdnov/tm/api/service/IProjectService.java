package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IService<Project> {

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

}
