package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}
