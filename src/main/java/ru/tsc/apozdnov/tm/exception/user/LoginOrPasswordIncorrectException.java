package ru.tsc.apozdnov.tm.exception.user;

public class LoginOrPasswordIncorrectException extends AbstractUserException {

    public LoginOrPasswordIncorrectException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}