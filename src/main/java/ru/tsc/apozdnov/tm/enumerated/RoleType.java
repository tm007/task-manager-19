package ru.tsc.apozdnov.tm.enumerated;

public enum RoleType {

    USUAL("Usual user"),
    ADMIN("Administrator");

    private final String displayName;

    RoleType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
