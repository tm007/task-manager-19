package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Date;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create project.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("**** PROJECT CREATE ****");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        System.out.println("ENTER DATE BEGIN: ");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END: ");
        final Date dateEnd = TerminalUtil.nextDate();
        serviceLocator.getProjectService().create(name, description, dateBegin, dateEnd);
    }

}
