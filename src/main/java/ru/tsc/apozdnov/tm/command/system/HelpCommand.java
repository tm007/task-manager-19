package ru.tsc.apozdnov.tm.command.system;

import ru.tsc.apozdnov.tm.api.model.ICommand;
import ru.tsc.apozdnov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Collections;

public final class HelpCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Display all commands";
    }

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommand();
        for (final AbstractCommand command : commands) System.out.println(command);
    }

}
